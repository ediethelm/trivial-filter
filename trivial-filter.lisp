;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license (unless declared otherwise).

(in-package :trivial-filter)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *trace-filter-method* nil))


(defvar *filter-readtable* nil)
(defvar *filter-costs* nil)

(define-condition query-failure (condition)
  ((bindings :initarg :bindings
	     :reader bindings)
   (proposition :initarg :proposition
		:reader proposition)
   (filter :initarg :filter
		:reader complete-filter)
   (continuation :initarg :continuation
		 :reader continuation))
  (:report (lambda (condition stream)
	     (format stream "The proposition '~A' could not be solved with the given bindings '~A'."
		     (proposition condition)
		     (bindings condition)))))

(define-condition query-failure/null-result (query-failure)
  ()
  (:report (lambda (condition stream)
	     (format stream "The proposition '~A' returned a NULL value with the given bindings '~A'."
		     (proposition condition)
		     (bindings condition)))))

(define-condition query-failure/terminate-empty-result (query-failure)
  ()
  (:report (lambda (condition stream)
	     (format stream "The proposition '~A' terminated with an empty result with the given bindings '~A'."
		     (proposition condition)
		     (bindings condition)))))

(define-condition query-failure/unknown-filter-method (query-failure)
  ()
  (:report (lambda (condition stream)
	     (format stream "No filter method defined for proposition '~A' with the given bindings '~A'."
		     (proposition condition)
		     (bindings condition)))))

(define-condition query-failure/invalid-argument (query-failure)
  ()
  (:report (lambda (condition stream)
	     (format stream "The proposition '~A' contains an invalid argument with the given bindings '~A'."
		     (proposition condition)
		     (bindings condition)))))

(defun clone-replacing-placeholders (filter &optional variable-cache)
  (let ((variable-cache (or variable-cache '(()))))
    (clone-replacing-placeholders% filter :variable-cache variable-cache)))

(defun clone-replacing-placeholders% (filter &key variable-cache)
  (if (null filter)
      nil
      (cons (cond
	      ((consp (car filter))
	       (clone-replacing-placeholders% (car filter) :variable-cache variable-cache))
	      ((symbolp (car filter))
	       (trivial-utilities:aif (assoc (car filter) (car variable-cache))
				      (cdr it)
				      (if (eq (char (symbol-name (car filter)) 0) #\?)
					  (trivial-utilities:aprog1
					      (make-instance 'trivial-variable-bindings:place-holder
							     :name (subseq (symbol-name (car filter)) 1))
					    (push (cons (car filter) it) (car variable-cache)))
					  (car filter))))
	      (t
	       (car filter)))
	    (clone-replacing-placeholders% (cdr filter) :variable-cache variable-cache))))

(defmacro signal-query-failure (reason filter bindings backtracker)
  `(signal ',(intern (concatenate 'string "QUERY-FAILURE/" (symbol-name reason)) :trivial-filter)
	   :proposition ,filter
	   :bindings ,bindings
	   :continuation ,backtracker))

(defun query (filter
	      &rest keys
	      &key backtracker initial-bindings emit-signal-on-failure
		&allow-other-keys)
  ""
  (declare (type boolean emit-signal-on-failure))
  (let* ((additional-keys (trivial-utilities:flatten keys))
	 (local-backtracker (or backtracker (make-instance 'trivial-continuation:continuation-result
						     :operation :terminate
						     :result nil
						     :continuation nil)))
	 (bindings (or initial-bindings
		       (trivial-continuation:result local-backtracker)
		       (make-instance 'trivial-variable-bindings:bindings))))
    (declare (type trivial-continuation:continuation-result local-backtracker)
	     (type trivial-variable-bindings:bindings bindings))

    (unless (consp filter)
      (return-from query local-backtracker))

    (setf (getf additional-keys :backtracker) local-backtracker)
    (setf (getf additional-keys :bindings) bindings)
    (setf (getf additional-keys :emit-signal-on-failure) emit-signal-on-failure)
    (let ((query-result (handler-bind
			    ((query-failure (lambda (c)
					      (when emit-signal-on-failure
						(signal c))
					      (invoke-restart 'terminate))))
			      (apply #'execute-query (clone-replacing-placeholders filter) :allow-other-keys t additional-keys))))
      (unless query-result
	(return-from query (make-instance 'trivial-continuation:continuation-result
					  :operation :terminate
					  :result nil
					  :continuation nil)))

      (unless (eq query-result local-backtracker)
	(setf (slot-value query-result 'trivial-continuation:operation) :combine))

      (return-from query query-result))))


(defun execute-query (filter
		      &rest keys
		      &key bindings backtracker
			&allow-other-keys)
  ""
  (declare (type trivial-continuation:continuation-result backtracker))

  (unless (consp filter)
    (return-from execute-query backtracker))

  (return-from execute-query
    (restart-case (trivial-utilities:aif (get-filter-method (car filter))
					 (let ((query-result (funcall it bindings (cdr filter) backtracker keys)))
					   (cond ((null query-result)
						  (signal-query-failure :null-result filter bindings backtracker))
						 ((and (eq :terminate (trivial-continuation:operation query-result))
						       (null (trivial-continuation:result query-result)))
						  (signal-query-failure :terminate-empty-result filter bindings backtracker))
						 ((eq :delegate (trivial-continuation:operation query-result))
						  backtracker)
						 ((null (trivial-continuation:result query-result))
						  (signal-query-failure :null-result filter bindings backtracker))
						 (t (trivial-continuation:combine-continuations query-result backtracker))))
					 (signal-query-failure :unknown-filter-method filter bindings backtracker))
      (terminate ()
	:report "End query with a termination result."
	(make-instance 'trivial-continuation:continuation-result
		       :operation :terminate
		       :result nil
		       :continuation nil))
      (use-value (value)
	:report "Use a new value as result."
	value))))

(defun get-filter-method (key)
  "Recover the filter method accociated to the given *KEY*."
  (cdr (assoc key *filter-readtable*)))

(defun add-filter-method (key func)
  "Save the accociation between the filter method *KEY* and its method *FUNC*."
  (trivial-utilities:aif (assoc key *filter-readtable*)
			 (setf (cdr it) func)
			 (push (cons key func) *filter-readtable*)))

(defun remove-filter-method (key)
  "Remove a filter method defined by *KEY* from the list of known filter methods."
  (setf *filter-readtable* (delete key *filter-readtable* :key #'car)))

(defun enable-filter-method-trace ()
  (setf *trace-filter-method* t))

(defun disable-filter-method-trace ()
  (setf *trace-filter-method* nil))

(defmacro create-filter-method (key &body body)
  "Create a filter method identified by *KEY* defined by *BODY*."
  (if *trace-filter-method*
      `(let ((filter-method (intern ,(concatenate 'string "FILTER-METHOD-" (symbol-name key)))))
	 (setf (symbol-function filter-method)
	       (trivial-continuation:cc/define-lambda (,(intern (string 'bindings)) ,(intern (string 'args)) ,(intern (string 'backtracker)) ,(intern (string 'keys)))
		 (declare (ignorable ,(intern (string 'keys)) ,(intern (string 'backtracker))))
		 ;; @TODO Protect from error conditions in *body* with a handler-case
		 ,@body))
	 (trace ,(intern (concatenate 'string "FILTER-METHOD-" (symbol-name key))))
	 (add-filter-method
	  ,key
	  filter-method))
      `(add-filter-method
	,key
	(trivial-continuation:cc/define-lambda (,(intern (string 'bindings)) ,(intern (string 'args)) ,(intern (string 'backtracker)) ,(intern (string 'keys)))
	  (declare (ignorable ,(intern (string 'keys)) ,(intern (string 'backtracker))))
	  ;; @TODO Protect from error conditions in *body* with a handler-case
	  ,@body))))

(defun unify-filter (lhs rhs bindings)
  "*lhs* -> the filter or query  
*rhs* -> the pattern to check against"
  ;; Symbols
  ;;; :? unbound variable
  ;;; :_ bound variable or constant
  ;;; :* bound or unbound variable or constant

  (when (and (symbolp rhs)
	     (consp lhs)
	     (equals rhs (car lhs)))
    (return-from unify-filter t))

  (unless (and (consp lhs) (consp rhs)
	       (eq (length lhs) (length rhs))
	       (eq (car lhs) (car rhs)))
    (return-from unify-filter nil))

  (labels ((unify (lhs rhs bindings)
	     (cond ((and (null lhs) (null rhs))
		    (return-from unify t))
		   ((not (and (consp lhs) (consp rhs)))
		    (return-from unify nil))
		   ((and (consp (car lhs)) (consp (car rhs))
			 (not (unify (car lhs) (car rhs) bindings)))
		    (return-from unify nil))
		   ((and (eq (car rhs) :?)
			 (not (and (typep (car lhs) 'trivial-variable-bindings:place-holder)
				   (eq (trivial-variable-bindings:bound-variable-value (car lhs) bindings) (car lhs)))))
		    (return-from unify nil))
		   ((and (eq (car rhs) :_)
			 (typep (car lhs) 'trivial-variable-bindings:place-holder)
			 (eq (trivial-variable-bindings:bound-variable-value (car lhs) bindings) (car lhs)))
		    (return-from unify nil))
		   ((eq (car rhs) :*)
		    nil) ;; do nothing in this case
		   ((and (not (member (car rhs) '(:? :_ :*)))
			 (typep (car lhs) 'trivial-variable-bindings:place-holder))
		    (return-from unify nil)))

	     (unify (cdr lhs) (cdr rhs) bindings)))
    
    (return-from unify-filter (the (values boolean &optional) (unify lhs rhs bindings)))))

(defun get-filter-cost (filter bindings &key (default 1000))
  "Read the execution cost of a *FILTER* in the context of the given *BINDINGS* (via unification). When no matching filter method is found after unification, the *DEFAULT* is returned."
  (trivial-utilities:aif (car
			  (sort
			   (the list (remove-if-not #'(lambda (rhs)
							(unify-filter filter
								      (car rhs)
								      bindings))
						    *filter-costs*))
			   #'< :key #'cdr))
			 (if (typep (cdr it) 'function)
			     (funcall (cdr it) filter bindings)
			     (cdr it))
			 default))

(defun set-filter-cost (filter cost)
  "Establish a mapping from the given *FILTER* to its execution *COST*.  
**filter** - A list which describes a filter (e.g. '(:eq ?X ?Y t))  
**cost** - An integer value which can be freely defined or a function of two arguments (the filter itself and bindings) returning an integer"
  (trivial-utilities:aif (assoc filter *filter-costs*)
			 (setf (cdr it) cost)
			 (setf *filter-costs* (push (cons filter cost) *filter-costs*))))

;;
;;;;------------ Filter Methods ------------
;;

(defun extract-value (place bindings)
  (if (typep place 'trivial-variable-bindings:place-holder)
      (let ((value (trivial-variable-bindings:bound-variable-value place bindings)))
	(if (typep value 'trivial-continuation:continuation-result)
	    (trivial-continuation:result value)
	    value))
      place))

(defun set-additional-keys (key value additional-keys)
  (trivial-utilities:aif (member key additional-keys)
			 (setf (cadr it) value)
			 (if (null additional-keys)
			     (list key value)
			     (nconc additional-keys (list key value))))
  (return-from set-additional-keys additional-keys))

(defun make-recursive-backtracker/and (args continuation previous-backtracker
				   &rest keys
				   &key dummy
				     &allow-other-keys)
  (declare (ignore dummy))
  (trivial-continuation:cc/define-lambda ()

    (when (eq (trivial-continuation:operation continuation) :terminate)
      (trivial-continuation:cc/return continuation))

    (let ((additional-keys (trivial-utilities:flatten keys))
	  (result (trivial-continuation:call-continuation continuation)))
      (trivial-utilities:aif (trivial-continuation:result result)
			     (let* ((new-backtracker
				     (make-instance 'trivial-continuation:continuation-result
						    :continuation (apply #'make-recursive-backtracker/or args result previous-backtracker additional-keys)
						    :result it
						    :operation :combine))

				    (additional-keys (set-additional-keys :allow-other-keys t
									  (set-additional-keys :backtracker new-backtracker additional-keys))))

			       (trivial-continuation:cc/return
				(apply #'execute-query args additional-keys)))
			     (trivial-continuation:cc/return (trivial-continuation:call-continuation previous-backtracker))))))

(defun make-recursive-backtracker/or (args continuation previous-backtracker
				   &rest keys
				   &key dummy
				     &allow-other-keys)
  (declare (ignore dummy))
  (trivial-continuation:cc/define-lambda ()

    (when (eq (trivial-continuation:operation continuation) :terminate)
      (trivial-continuation:cc/return continuation))

    (let ((additional-keys (trivial-utilities:flatten keys))
	  (result (trivial-continuation:call-continuation continuation)))
      (trivial-utilities:aif (trivial-continuation:result result)
			     (trivial-continuation:cc/create-return it
								    (apply #'make-recursive-backtracker/or
									   (cons :or (cdr args))
									   result
									   previous-backtracker additional-keys))
			     (trivial-continuation:cc/return (apply #'execute-query (cons :or (cdr args)) additional-keys))))))

(create-filter-method :and
  (unless args
    (trivial-continuation:cc/delegate))

  (let* ((additional-keys (set-additional-keys :allow-other-keys t
					       (set-additional-keys :bindings bindings
								    (set-additional-keys :backtracker backtracker
											 (trivial-utilities:flatten keys)))))
	 (query-result (apply #'execute-query (car args) additional-keys)))

    (unless (cdr args)
      (trivial-continuation:cc/return query-result))

    (iterate:iterate
      (iterate:while (trivial-continuation:result query-result))

      ;; Replace backtracker in additional-keys
      (setf (getf additional-keys :backtracker) (make-instance 'trivial-continuation:continuation-result
							       :continuation nil
							       :operation :terminate
							       :result nil))

      (setf (getf additional-keys :bindings) (trivial-continuation:result query-result))

      ;; This is used to continue traversing the query
      (let ((result (apply #'execute-query (cons :and (cdr args)) additional-keys)))
	(when (eq (trivial-continuation:operation result) :delegate)
	  (trivial-continuation:cc/return query-result))
	(if (trivial-continuation:result result)
	    (trivial-continuation:cc/create-return (trivial-continuation:result result)
						   (make-recursive-backtracker/and (cons :and (cdr args)) result query-result))
	    (trivial-continuation:cc/continue query-result)))))

  (trivial-continuation:cc/return backtracker))

(create-filter-method :or
  (unless args
    (trivial-continuation:cc/delegate))

  (let* ((additional-keys (set-additional-keys :allow-other-keys t
					       (set-additional-keys :bindings bindings
								    (set-additional-keys :backtracker backtracker
											 (trivial-utilities:flatten keys)))))
	 (query-result (apply #'execute-query (car args) additional-keys)))

    (unless (cdr args)
      (trivial-continuation:cc/return query-result))

    (if (trivial-continuation:result query-result)
	(trivial-continuation:cc/create-return (trivial-continuation:result query-result)
					       (apply #'make-recursive-backtracker/or (cons :or (cdr args))
						      query-result
						      (make-instance 'trivial-continuation:continuation-result
								     :continuation nil
								     :operation :terminate
								     :result nil)
						      additional-keys))
	(if (cdr args)
	    (trivial-continuation:cc/return (apply #'execute-query (cons :or (cdr args)) additional-keys))
	    (trivial-continuation:cc/delegate)))))

(create-filter-method :not
  (let ((val (handler-bind
		 ((query-failure (lambda (c)
				   (declare (ignore c))
				   (invoke-restart 'terminate))))
	       (apply #'execute-query bindings (first args) (set-additional-keys :allow-other-keys t
										 (set-additional-keys :backtracker backtracker
												      (trivial-utilities:flatten keys)))))))

    (if (trivial-continuation:result val)
	(signal-query-failure :terminate-empty-result (append '(:not) args) bindings backtracker)
	(trivial-continuation:cc/create-return bindings (trivial-continuation:continuation val) :operation :combine))))

(create-filter-method :contains
  ;; :contains expects 3 agruments:
  ;;  1. list
  ;;  2. object
  ;;  3. result, a boolean (found/not found)
  ;; Each one can be a placeholder, but not all three simultameously.
  (let ((container (extract-value (first args) bindings))
	(obj (extract-value (second args) bindings))
	(result (extract-value (third args) bindings)))
    (cond ((null container)
	   (cond ((typep result 'trivial-variable-bindings:place-holder) ;; @REVIEW Failure as negation
		  (trivial-continuation:cc/create-return (progn
							   (setf (trivial-variable-bindings:bound-variable-value result bindings) nil)
							   bindings)
							 nil))
		 ((null result)
		  (trivial-continuation:cc/create-return bindings nil))
		 (t
		  (signal-query-failure :terminate-empty-result (append '(:contains) args) bindings backtracker))))
	  ((typep container 'trivial-variable-bindings:place-holder)
	   (signal-query-failure :invalid-argument (append '(:contains) args) bindings backtracker))
	  ((typep obj 'trivial-variable-bindings:place-holder)
	   (let ((bindings-copy (trivial-utilities:clone bindings)))
	     (setf (trivial-variable-bindings:bound-variable-value obj bindings-copy) (car container))
	     (trivial-continuation:cc/create-return (progn
						      (when (typep result 'trivial-variable-bindings:place-holder)
							(setf (trivial-variable-bindings:bound-variable-value result bindings-copy) t))
						      bindings-copy)
						    #'(lambda () (self bindings ;; @TODO How to signal a query-failure when this reaches the end?
								       (list (cdr container) obj (third args))
								       backtracker
								       keys)))))
	  (t
	   (trivial-utilities:extract-additional-keys (((consider-revisions nil))
						       keys)
	     (let ((bindings-copy (trivial-utilities:clone bindings)))
	       (trivial-continuation:cc/create-return (if (member obj container :test #'(lambda (x y)
											  (trivial-utilities:equals x y :consider-revisions consider-revisions)))
							  (if (typep result 'trivial-variable-bindings:place-holder)
							      (progn
								(setf (trivial-variable-bindings:bound-variable-value result bindings-copy) t)
								bindings)
							      (if result bindings nil))
							  (if (typep result 'trivial-variable-bindings:place-holder)
							      (progn
								(setf (trivial-variable-bindings:bound-variable-value result bindings-copy) nil)
								bindings)
							      (if result nil bindings-copy)))
						      nil)))))))

(create-filter-method :lt
  ;; :lt expects 3 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;;  3. result, a boolean
  ;; Each one can be a placeholder, but not all three simultaneously.
  (let ((lhs (extract-value (first args) bindings))
	(rhs (extract-value (second args) bindings))
	(result (extract-value (third args) bindings))
	(value bindings))
    
    (cond ((or (typep lhs 'trivial-variable-bindings:place-holder)
	       (typep rhs 'trivial-variable-bindings:place-holder))
	   (signal-query-failure :invalid-argument (append '(:lt) args) bindings backtracker))
	  ((typep result 'trivial-variable-bindings:place-holder)
	   (if (and (numberp lhs) (numberp rhs))
	       (progn
		 (setf value (trivial-utilities:clone bindings))
		 (setf (trivial-variable-bindings:bound-variable-value (third args) value) (< lhs rhs)))
	       (progn
		 (signal-query-failure :invalid-argument (append '(:lt) args) bindings backtracker)
		 (setf value nil))))
	  (t
	   (if (and (numberp lhs) (numberp rhs))
	       (unless (eq (< lhs rhs) result)
		 (setf value nil))
	       (progn
		 (signal-query-failure :invalid-argument (append '(:lt) args) bindings backtracker)
		 (setf value nil)))))

    (if value
	(trivial-continuation:cc/create-return value nil :operation :combine)
	(signal-query-failure :terminate-empty-result (append '(:lt) args) bindings backtracker))))

(create-filter-method :le
  ;; :le expects 3 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;;  3. result, a boolean
  ;; Each one can be a placeholder, but not all three simultameously.
  (let ((lhs (extract-value (first args) bindings))
	(rhs (extract-value (second args) bindings))
	(result (extract-value (third args) bindings))
	(value bindings))
    
    (cond ((or (typep lhs 'trivial-variable-bindings:place-holder)
	       (typep rhs 'trivial-variable-bindings:place-holder))
	   (setf value nil))
	  ((typep result 'trivial-variable-bindings:place-holder)
	   (if (and (numberp lhs) (numberp rhs))
	       (progn
		 (setf value (trivial-utilities:clone bindings))
		 (setf (trivial-variable-bindings:bound-variable-value (third args) value) (<= lhs rhs)))
	       (progn
		 (signal-query-failure :invalid-argument (append '(:le) args) bindings backtracker)
		 (setf value nil))))
	  (t
	   (if (and (numberp lhs) (numberp rhs))
	       (unless (eq (<= lhs rhs) result)
		 (setf value nil))
	       (progn
		 (signal-query-failure :invalid-argument (append '(:le) args) bindings backtracker)
		 (setf value nil)))))

    (if value
	(trivial-continuation:cc/create-return value nil :operation :combine)
	(signal-query-failure :terminate-empty-result (append '(:le) args) bindings backtracker))))

(create-filter-method :eq
  ;; :eq expects 2 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;; Each one can be a placeholder, but not all three simultameously.
  (trivial-utilities:extract-additional-keys (((consider-revisions nil))
					      keys)
  (let ((lhs (extract-value (first args) bindings))
	(rhs (extract-value (second args) bindings))
	(equality (extract-value (third args) bindings))
	(value bindings))
      (cond ((and (typep lhs 'trivial-variable-bindings:place-holder)
		  (typep rhs 'trivial-variable-bindings:place-holder))
	     ;; @TODO If both lhs and rhs are variables and (third args) is t, then set bindings to ((lhs . rhs) (rhs . lhs))
	     (setf value nil))
	    ((and equality (typep lhs 'trivial-variable-bindings:place-holder))
	     (setf value (trivial-utilities:clone bindings))
	     (setf (trivial-variable-bindings:bound-variable-value lhs value) rhs))
	    ((and equality (typep rhs 'trivial-variable-bindings:place-holder))
	     (setf value (trivial-utilities:clone bindings))
	     (setf (trivial-variable-bindings:bound-variable-value rhs value) lhs))
	    ((typep equality 'trivial-variable-bindings:place-holder)
	     (setf value (trivial-utilities:clone bindings))
	     (setf (trivial-variable-bindings:bound-variable-value equality value)
		   (trivial-utilities:equals lhs rhs :consider-revisions consider-revisions)))
	    ((and (typep rhs 'trivial-variable-bindings:place-holder)
		  (not (trivial-utilities:equals lhs rhs :consider-revisions consider-revisions)))
	     (setf value nil))
	    (t
	     (setf value nil)))

    (if value
	(trivial-continuation:cc/create-return value nil :operation :combine)
	(signal-query-failure :terminate-empty-result (append '(:eq) args) bindings backtracker)))))

(create-filter-method :ge
  ;; :ge expects 3 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;;  3. result, a boolean
  ;; Each one can be a placeholder, but not all three simultameously.
  (let ((lhs (extract-value (first args) bindings))
	(rhs (extract-value (second args) bindings))
	(result (extract-value (third args) bindings))
	(value bindings))
    
    (cond ((or (typep lhs 'trivial-variable-bindings:place-holder)
	       (typep rhs 'trivial-variable-bindings:place-holder))
	   (setf value nil))
	  ((typep result 'trivial-variable-bindings:place-holder)
	   (if (and (numberp lhs) (numberp rhs))
	       (progn
		 (setf value (trivial-utilities:clone bindings))
		 (setf (trivial-variable-bindings:bound-variable-value (third args) value) (>= lhs rhs)))
	       (progn
		 (signal-query-failure :invalid-argument (append '(:ge) args) bindings backtracker)
		 (setf value nil))))
	  (t
	   (if (and (numberp lhs) (numberp rhs))
	       (unless (eq (>= lhs rhs) result)
		 (setf value nil))
	       (progn
		 (signal-query-failure :invalid-argument (append '(:ge) args) bindings backtracker)
		 (setf value nil)))))
    
    (if value
	(trivial-continuation:cc/create-return value nil :operation :combine)
	(signal-query-failure :terminate-empty-result (append '(:ge) args) bindings backtracker))))

(create-filter-method :gt
  ;; :gt expects 3 agruments:
  ;;  1. value_1
  ;;  2. value_2
  ;;  3. result, a boolean
  ;; Each one can be a placeholder, but not all three simultameously.
  (let ((lhs (extract-value (first args) bindings))
	(rhs (extract-value (second args) bindings))
	(result (extract-value (third args) bindings))
	(value bindings))

    (cond ((or (typep lhs 'trivial-variable-bindings:place-holder)
	       (typep rhs 'trivial-variable-bindings:place-holder))
	   (setf value nil))
	  ((typep result 'trivial-variable-bindings:place-holder)
	   (if (and (numberp lhs) (numberp rhs))
	       (progn
		 (setf value (trivial-utilities:clone bindings))
		 (setf (trivial-variable-bindings:bound-variable-value (third args) value) (> lhs rhs)))
	       (progn
		 (signal-query-failure :invalid-argument (append '(:gt) args) bindings backtracker)
		 (setf value nil))))
	  (t
	   (if (and (numberp lhs) (numberp rhs))
	       (unless (eq (> lhs rhs) result)
		 (setf value nil))
	       (progn
		 (signal-query-failure :invalid-argument (append '(:gt) args) bindings backtracker)
		 (setf value nil)))))

    (if value
	(trivial-continuation:cc/create-return value nil :operation :combine)
	(signal-query-failure :terminate-empty-result (append '(:gt) args) bindings backtracker))))



#|
 Add filters for:

   - arithmetic (plus minus multiply divide abs sum)
   - string manipulation (concatenate split replace) + comparison in existing (lt le eq ge gt)
|#


(set-filter-cost :and #'(lambda (filter bindings) (+ 10 (reduce #'+ (cdr filter) :key #'(lambda (x) (get-filter-cost x bindings))))))
(set-filter-cost :or #'(lambda (filter bindings) (+ 10 (reduce #'+ (cdr filter) :key #'(lambda (x) (get-filter-cost x bindings))))))
(set-filter-cost :not #'(lambda (filter bindings) (+ 10 (get-filter-cost (cadr filter) bindings))))
(set-filter-cost '(:eq :_ :_ :*) 1)
(set-filter-cost '(:eq :? :_ t) 1)
(set-filter-cost '(:eq :_ :? t) 1)
(set-filter-cost '(:eq :_ :_ :*) 1)
(set-filter-cost '(:contains :_ :? :*) 50)
(set-filter-cost '(:contains :_ :_ :?) 15)
(set-filter-cost '(:contains :_ :? :_) 10)
(set-filter-cost '(:lt :_ :_ :*) 1)
(set-filter-cost '(:le :_ :_ :*) 1)
(set-filter-cost '(:ge :_ :_ :*) 1)
(set-filter-cost '(:gt :_ :_ :*) 1)

