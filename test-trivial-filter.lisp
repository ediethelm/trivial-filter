(in-package :trivial-filter)

(5am:def-suite :trivial-filter-tests :description "Trivial Filter tests")
(5am:in-suite :trivial-filter-tests)

(defun get-value (var result)
  (trivial-variable-bindings:bound-variable-value var (trivial-continuation:result result)))

(5am:test filter-method-contains
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (container '(1 2 3 5 8 13))
	 (res (trivial-filter:query `(:contains ,container ,var-x t))))

    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 0 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 1 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 2 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 3 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 4 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 5 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (null (trivial-continuation:result res))))

  (let* ((res (trivial-filter:query `(:contains (x y 1 4 a b 6) a ,t))))
    (5am:is (not (null (trivial-continuation:result res)))))

  (let* ((res (trivial-filter:query `(:contains (x y 1 4 a b 6) a ,nil))))
    (5am:is (null (trivial-continuation:result res))))
  
  (let* ((res (trivial-filter:query `(:contains (x y 1 4 a b 6) c ,t))))
    (5am:is (null (trivial-continuation:result res))))

  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (res (trivial-filter:query `(:contains ,var-x c ,t))))
    (5am:is (null (trivial-continuation:result res))))

  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y (make-instance 'trivial-variable-bindings:place-holder :name "Y"))
	 (res (trivial-filter:query `(:contains (x y 1 4 a b 6) ,var-x ,var-y))))
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) 'x))
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-y (trivial-continuation:result res)) t))))

(5am:test filter-method-lt
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y(make-instance 'trivial-variable-bindings:place-holder :name "Y")))

    (let ((res (query `(:lt ,var-x ,var-y t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:lt 5 ,var-y t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:lt ,var-x 5 t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:lt 4 5 ,var-x))))
      (5am:is (eq (get-value var-x res) t)))

    (let ((res (query `(:lt 7 5 ,var-x))))
      (5am:is (eq (get-value var-x res) nil)))

    (let ((res (query `(:lt 7 7 ,var-x))))
      (5am:is (eq (get-value var-x res) nil)))

    (let ((res (query `(:lt "a" "b" ,var-x))))
      (5am:is (null (trivial-continuation:result res))))))

(5am:test filter-method-le
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y(make-instance 'trivial-variable-bindings:place-holder :name "Y")))

    (let ((res (query `(:le ,var-x ,var-y t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:le 5 ,var-y t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:le ,var-x 5 t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:le 4 4 ,var-x))))
      (5am:is (eq (get-value var-x res) t)))

    (let ((res (query `(:le 4 5 ,var-x))))
      (5am:is (eq (get-value var-x res) t)))

    (let ((res (query `(:le 7 5 ,var-x))))
      (5am:is (eq (get-value var-x res) nil)))))

(5am:test filter-method-eq
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y(make-instance 'trivial-variable-bindings:place-holder :name "Y")))

    (let ((res (query `(:eq ,var-x ,var-y t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:eq 5 ,var-y t))))
      (5am:is (eq (get-value var-y res) 5)))

    (let ((res (query `(:eq ,var-x 5 t))))
      (5am:is (eq (get-value var-x res) 5)))

    (let ((res (query `(:eq ,var-x 5 nil))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:eq 4 4 ,var-x))))
      (5am:is (eq (get-value var-x res) t)))

    (let ((res (query `(:eq "5" 5 ,var-x))))
      (5am:is (eq (get-value var-x res) nil)))))

(5am:test filter-method-ge
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y(make-instance 'trivial-variable-bindings:place-holder :name "Y")))

    (let ((res (query `(:ge ,var-x ,var-y t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:ge 5 ,var-y t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:ge ,var-x 5 t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:ge 4 4 ,var-x))))
      (5am:is (eq (get-value var-x res) t)))

    (let ((res (query `(:ge 4 5 ,var-x))))
      (5am:is (eq (get-value var-x res) nil)))

    (let ((res (query `(:ge 7 5 ,var-x))))
      (5am:is (eq (get-value var-x res) t)))))

(5am:test filter-method-gt
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y (make-instance 'trivial-variable-bindings:place-holder :name "Y")))

    (let ((res (query `(:gt ,var-x ,var-y t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:gt 5 ,var-y t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:gt ,var-x 5 t))))
      (5am:is (null (trivial-continuation:result res))))

    (let ((res (query `(:gt 4 5 ,var-x))))
      (5am:is (eq (get-value var-x res) nil)))

    (let ((res (query `(:gt 5 5 ,var-x))))
      (5am:is (eq (get-value var-x res) nil)))

    (let ((res (query `(:gt 7 5 ,var-x))))
      (5am:is (eq (get-value var-x res) t)))

    (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	   (var-y (make-instance 'trivial-variable-bindings:place-holder :name "Y"))
	   (bindings (make-instance 'trivial-variable-bindings:bindings)))
      (setf (trivial-variable-bindings:bound-variable-value var-x bindings) 5)
      (setf (trivial-variable-bindings:bound-variable-value var-y bindings) 5)
      (let ((result (trivial-filter:query `(:gt 3 4 t) :backtracker (make-instance 'trivial-continuation:continuation-result
										   :continuation nil
										   :operation :terminate
										   :result bindings))))
	(5am:is (null (trivial-continuation:result result)))))))

(5am:test filter-method-and
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (result (query `(:and (:contains (1 2 3 5 8 13) ,var-x t)
			       (:gt ,var-x 4 t)))))
    (5am:is (eq (get-value var-x result) 5))
    (trivial-continuation:cc/continue result)
    (5am:is (eq (get-value var-x result) 8))
    (trivial-continuation:cc/continue result)
    (5am:is (eq (get-value var-x result) 13))
    (trivial-continuation:cc/continue result)
    (5am:is (null (trivial-continuation:result result)))))

(5am:test filter-method-or
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (result (query `(:or (:eq ,var-x 1 t)
			      (:eq ,var-x 2 t)
			      (:eq ,var-x 3 t)
			      (:eq ,var-x 5 t)
			      (:eq ,var-x 8 t)
			      (:eq ,var-x 13 t)))))

    (5am:is (eq (get-value var-x result) 1))
    (trivial-continuation:cc/continue result)
    (5am:is (eq (get-value var-x result) 2))
    (trivial-continuation:cc/continue result)
    (5am:is (eq (get-value var-x result) 3))
    (trivial-continuation:cc/continue result)
    (5am:is (eq (get-value var-x result) 5))
    (trivial-continuation:cc/continue result)
    (5am:is (eq (get-value var-x result) 8))
    (trivial-continuation:cc/continue result)
    (5am:is (eq (get-value var-x result) 13))
    (trivial-continuation:cc/continue result)
    (5am:is (null (trivial-continuation:result result)))))

(5am:test unify-filter
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y (make-instance 'trivial-variable-bindings:place-holder :name "Y"))
	 (lhs `(:eq ,var-x ,var-y ,t))
	 (obj-a 'abc)
	 (obj-b nil)
	 (bindings (make-instance 'trivial-variable-bindings:bindings)))
    
    (5am:is (eq (unify-filter lhs '(:eq :?  :?  :*) bindings) t))
    (5am:is (eq (unify-filter lhs '(:eq :?  :?  :_) bindings) t))
    (5am:is (eq (unify-filter lhs '(:eq :?  :?  :?) bindings) nil))
    (5am:is (eq (unify-filter lhs '(:eq :*  :*  :*) bindings) t))
    (5am:is (eq (unify-filter lhs '(:eq :*  :_  :*) bindings) nil))

    (setf (trivial-variable-bindings:bound-variable-value var-x bindings) obj-b)
    (5am:is (eq (unify-filter lhs '(:eq :_  :?  :*) bindings) t))
    (5am:is (eq (unify-filter lhs '(:eq :_  :_  :*) bindings) nil))

    (setf (trivial-variable-bindings:bound-variable-value var-y bindings) obj-a)
    (5am:is (eq (unify-filter lhs '(:eq :_  :_  :*) bindings) t))
    (5am:is (eq (unify-filter lhs '(:eq :?  :_  :*) bindings) nil))
    (5am:is (eq (unify-filter lhs '(:eq :?  :?  :*) bindings) nil))

    (5am:is (eq (unify-filter lhs '(:eq :_  :_  :?) bindings) nil))
    (5am:is (eq (unify-filter lhs '(:eq :_  :_  :_) bindings) t))

    (5am:is (eq (unify-filter lhs '(:contains :*  :*  :*) bindings) nil))
    (5am:is (eq (unify-filter lhs '(:not :* :*) bindings) nil))

    (5am:is (eq (unify-filter (clone-replacing-placeholders '(:eigenschaft :?1 :id :?2))  '(:eigenschaft :?  :*  :*) bindings) t))))

(5am:test filter-cost
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y (make-instance 'trivial-variable-bindings:place-holder :name "Y"))
	 (bindings (make-instance 'trivial-variable-bindings:bindings)))

    (5am:is (eq (get-filter-cost `(:eq ,var-x ,var-y t) bindings) 1000))
    (5am:is (eq (get-filter-cost `(:eq t t t) bindings) 1))
    (5am:is (eq (get-filter-cost `(:eq t ,var-y t) bindings) 1))
    (5am:is (eq (get-filter-cost `(:eq ,var-x t t) bindings) 1))
    (5am:is (eq (get-filter-cost `(:lt ,var-x 10 t) bindings) 1000))

    (setf (trivial-variable-bindings:bound-variable-value var-x bindings) 123)
    (setf (trivial-variable-bindings:bound-variable-value var-y bindings) 456)
    (5am:is (eq (get-filter-cost `(:eq ,var-x ,var-y t) bindings) 1))))

(5am:test filter-cost-sorting
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y (make-instance 'trivial-variable-bindings:place-holder :name "Y"))
	 (obj-a 'abc)
	 (bindings (make-instance 'trivial-variable-bindings:bindings))
	 (filters nil))

    (labels ((best-filter (filters bindings)
	       (iterate:iterate
		 (iterate:for filter in filters)
		 (iterate:for cost = (get-filter-cost filter bindings))
		 (iterate:finding (cons filter cost) minimizing cost))))

      (push `(:lt ,var-x ,var-y t) filters)
      (push `(:ge ,var-x ,var-y t) filters)
      (push `(:eq ,var-x ,var-y t) filters)
      (push `(:contains (a b c) ,var-x ,var-y) filters)

      (let ((best (best-filter filters bindings)))
	(5am:is (eq (car best) (nth 0 filters)))
	(5am:is (eq (cdr best) 50)))

      (setf (trivial-variable-bindings:bound-variable-value var-y bindings) obj-a)

      (let ((best (best-filter filters bindings)))
	(5am:is (eq (car best) (nth 1 filters)))
	(5am:is (eq (cdr best) 1))))))

;; ------- with emit-signal-on-failure ------

(5am:test filter-method-contains/signaling
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (container '(1 2 3 5 8 13))
	 (res (trivial-filter:query `(:contains ,container ,var-x t) :emit-signal-on-failure t)))

    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 0 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 1 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 2 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 3 container)))
    (trivial-continuation:cc/continue res)
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 4 container)))
    (5am:finishes (trivial-continuation:cc/continue res))
    (5am:is (eq (trivial-variable-bindings:bound-variable-value var-x (trivial-continuation:result res)) (nth 5 container)))
    
    (5am:signals query-failure/terminate-empty-result
      (trivial-continuation:cc/continue res)))

  (5am:signals query-failure/null-result
    (trivial-filter:query `(:contains (x y 1 4 a b 6) a ,nil) :emit-signal-on-failure t))
  
  (5am:signals query-failure/null-result
    (trivial-filter:query `(:contains (x y 1 4 a b 6) c ,t) :emit-signal-on-failure t))

  (let ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X")))
    (5am:signals query-failure/invalid-argument
      (trivial-filter:query `(:contains ,var-x c ,t) :emit-signal-on-failure t))))


(5am:test filter-method-lt/signaling
  (let* ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	 (var-y (make-instance 'trivial-variable-bindings:place-holder :name "Y")))

    (5am:signals query-failure/invalid-argument (query `(:lt ,var-x ,var-y t) :emit-signal-on-failure t))
    (5am:signals query-failure/invalid-argument (query `(:lt 5 ,var-y t) :emit-signal-on-failure t))
    (5am:signals query-failure/invalid-argument (query `(:lt ,var-x 5 t) :emit-signal-on-failure t))

    (let ((res (query `(:lt 4 5 ,var-x) :emit-signal-on-failure t)))
      (5am:is (eq (get-value var-x res) t)))

    (let ((res (query `(:lt 7 5 ,var-x) :emit-signal-on-failure t)))
      (5am:is (eq (get-value var-x res) nil)))

    (let ((res (query `(:lt 7 7 ,var-x) :emit-signal-on-failure t)))
      (5am:is (eq (get-value var-x res) nil)))

    (5am:signals query-failure/invalid-argument (query `(:lt "a" "b" ,var-x) :emit-signal-on-failure t))))


(defun replace-variables (proposition bindings)
  (iterate:iterate
    (iterate:for elm in proposition)
    (iterate:collecting
     (cond ((consp elm)
	    (replace-variables elm bindings))
	   ((typep elm 'trivial-variable-bindings:place-holder)
	    (trivial-variable-bindings:bound-variable-value elm bindings))
	   (t
	    elm)))))

(5am:test signaling/unknown-filter-method
  (let ((var-x (make-instance 'trivial-variable-bindings:place-holder :name "X")))
    
    (5am:signals query-failure/unknown-filter-method (trivial-filter:query `(:add 3 5 ,var-x) :emit-signal-on-failure t))

    (let ((result (handler-case
		      (trivial-filter:query `(:add 3 5 ,var-x) :emit-signal-on-failure t)
		    (query-failure/unknown-filter-method (condition)
		      (let* ((bindings (trivial-utilities:clone (bindings condition)))
			    (proposition (replace-variables (proposition condition) bindings)))
			(setf (trivial-variable-bindings:bound-variable-value (fourth proposition) bindings)
			      (+ (second proposition) (third proposition)))
			(make-instance 'trivial-continuation:continuation-result
				       :result bindings
				       :continuation nil
				       :operation :combine))))))
      (5am:is (eq (get-value var-x result) (+ 3 5)))))

  (let ((var-i (make-instance 'trivial-variable-bindings:place-holder :name "I"))
	(var-x (make-instance 'trivial-variable-bindings:place-holder :name "X"))
	(var-y (make-instance 'trivial-variable-bindings:place-holder :name "Y")))
    (let ((result (handler-bind
		      ((query-failure/unknown-filter-method
			(lambda (condition)
			  (invoke-restart 'use-value
					  (let* ((bindings (trivial-utilities:clone (bindings condition)))
						 (proposition (replace-variables (proposition condition) bindings)))
					    (cond ((eq :add (car (proposition condition)))
						   (setf (trivial-variable-bindings:bound-variable-value (fourth proposition) bindings)
							 (+ (second proposition) (third proposition)))
						   (make-instance 'trivial-continuation:continuation-result
								  :result bindings
								  :continuation nil
								  :operation :combine))
						  (t (log:error "Unhandled proposition '~A'." proposition)
						     (error "Unhandled proposition '~A'." proposition))))))))

		    (trivial-filter:query `(:and (:eq 5 ,var-i t) (:add 3 ,var-i ,var-x) (:lt ,var-x 9 t) (:gt ,var-x 7 ,var-y)) :emit-signal-on-failure t))))

      (5am:is (eq (get-value var-i result) 5))
      (5am:is (eq (get-value var-x result) (+ 3 5)))
      (5am:is (eq (get-value var-y result) t)))))
