(uiop:define-package #:trivial-filter
  (:documentation "")
  (:use #:common-lisp)
  (:import-from :trivial-utilities #:equals #:clone)
  (:export #:query
	   #:get-filter-method
	   #:add-filter-method
	   #:remove-filter-method
	   #:create-filter-method
	   #:enable-filter-method-trace
	   #:disable-filter-method-trace
	   #:extract-value
	   #:get-filter-cost
	   #:set-filter-cost
	   #:query-failure
	   #:query-failure/null-result
	   #:query-failure/terminate-empty-result
	   #:query-failure/unknown-filter-method
	   #:query-failure/invalid-argument
	   #:terminate
	   #:use-value
	   #:proposition
	   #:bindings
	   #:continuation
	   #:filter))

