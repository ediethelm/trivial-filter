# Trivial Filter Manual

###### \[in package TRIVIAL-FILTER\]
[![pipeline status](https://gitlab.com/ediethelm/trivial-filter/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/trivial-filter/commits/master)
[![Quicklisp](http://quickdocs.org/badge/trivial-filter.svg)](http://quickdocs.org/trivial-filter/)
[![coverage report](https://gitlab.com/ediethelm/trivial-filter/badges/master/coverage.svg?job=test-coverage)](https://gitlab.com/ediethelm/trivial-filter/-/jobs/artifacts/master/browse?job=test-coverage)


## Description

NOTE: This documentation is work-in-progress and will be extended whenever I find some time for it. Meanwhile the code is quite stable and being used.

## Filter Methods

Filter Methods must return a trivial-continuation.

Filter Methods has an associated execution cost.

## Filters

Variable  
  Bound Variable  
  Fact  
  Query  

Special symbols  
    :?  unbound variable  
    :\_  bound variable or constant  
    :\*  bound or unbound variable or constant

## Predefined Filter Methods

:lt        - Compare two values, checking if the first one is lower that the second. The values must be comparable via #'<  
  :le        - Compare two values, checking if the first one is lower or equal to the second. The values must be comparable via #'<=  
  :eq        - Compare two values, checking if the first one is less/lower that the second. The values must be comparable via #'trivial-utilities:equals  
  :ge        - Compare two values, checking if the first one is higher that the second. The values must be comparable via #'>=  
  :gt        - Compare two values, checking if the first one is higher or equal to the second. The values must be comparable via #'>  
  :contains  - If both first and second arguments are bound variables or constants, check if the first is a #'member of the second using #'trivial-utilities:equals as a test  
  :not       -  
  :and       -  
  :or        -  

## Examples

(:and (:lt :\_ 100)
      (:gt :\_ 1))

## Installing trivial-filter

This project is available in the latest [QuickLisp](https://www.quicklisp.org/beta/ "QuickLisp") distribution, so installing it is reduced to calling:

```lisp
(ql:quickload :trivial-filter)
```


## Working Example



## Exported Symbols

- [function] APPLY-FILTER BINDINGS FILTER &REST KEYS &KEY (BACKTACKER (MAKE-INSTANCE 'TRIVIAL-CONTINUATION:CONTINUATION-RESULT :OPERATION :TERMINATE
               :RESULT NIL :CONTINUATION NIL)) &ALLOW-OTHER-KEYS



- [function] GET-FILTER-METHOD KEY

    Recover the filter method accociated to the given *KEY*.

- [function] ADD-FILTER-METHOD KEY FUNC

    Save the accociation between the filter method *KEY* and its method *FUNC*.

- [function] REMOVE-FILTER-METHOD KEY

    Remove a filter method defined by *KEY* from the list of known filter methods.

- [macro] CREATE-FILTER-METHOD KEY &BODY BODY

    Create a filter method identified by *KEY* defined by *BODY*.

- [function] GET-FILTER-COST FILTER BINDINGS &KEY (DEFAULT 1000)

    Read the execution cost of a *FILTER* in the context of the given *BINDINGS* (via unification). When no matching filter method is found after unification, the *DEFAULT* is returned.

- [function] SET-FILTER-COST FILTER COST

    Establish a mapping from the given *FILTER* to its execution *COST*.  
    **filter** - A list which describes a filter (e.g. '(:eq ?X ?Y t))  
    **cost** - An integer value which can be freely defined

## License Information

This library is released under the MIT License. Please refer to the [LICENSE](https://gitlab.com/ediethelm/trivial-filter/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/ediethelm/trivial-filter/blob/master/CONTRIBUTING.md "Contributing") document for more information.
