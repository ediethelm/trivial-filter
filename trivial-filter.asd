;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :trivial-filter
  :name "trivial-filter"
  :description ""
  :long-description ""
  :version "0.4.0"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-utilities
	       :trivial-continuation
	       :trivial-variable-bindings)
  :in-order-to ((test-op (test-op :trivial-filter/test)))
  :components ((:file "package")
	       (:file "trivial-filter")))

(defsystem :trivial-filter/test
  :name "trivial-filter/test"
  :description "Unit Tests for the trivial-filter project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-filter
	       :fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :trivial-filter-tests))
  :components ((:file "test-trivial-filter")))

